<?php
/**
 * Application index
 *
 * 
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category   Relax
 * @copyright  Copyright (c) 2012, João Pinto Neto
 * @license    MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link       http://joaopintoneto.com/Relax
 * @since      Class available since Release 0.1
 */

//---------- debug ---------------------------
error_reporting(E_ALL);
ini_set('display_errors','On');
header('Content-Type: text/plain');
//---------- debug ---------------------------

define('RELAX_PATH', realpath('./Lib'));
include 'bootstrap.php';

new Relax_Router;