<?php
/**
 * Config
 *
 * 
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category   Relax
 * @package    Relax
 * @subpackage Config
 * @copyright  Copyright (c) 2012, João Pinto Neto
 * @license    MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link       http://joaopintoneto.com/Relax
 * @since      Class available since Release 0.1
 */
class Relax_Config {
    private static $__config = array();

    public static function loadFile($file) {
        include $file . '.php';
        self::$__config = array_merge(self::$__config, $config);
        return self::$__config;
    }

    public static function read($context) {
        $pieces = explode('.', $context);
        $output = &self::$__config;

        foreach ($pieces as $piece) {
            $output = &$output[$piece];
        }

        return $output;
    }

    public static function write($context, $value) {
        $pieces = explode('.', $context);
        $output = &self::$__config;

        foreach ($pieces as $piece) {
            $output[$piece] = null;
            if ($piece == end($pieces)) {
                $output[$piece] = $value;
            }
            $output = &$output[$piece];
        }
    }

    public static function drop() {
        self::$__config = array();
    }

}