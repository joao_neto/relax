<?php
/**
 * Router
 *
 * 
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category   Relax
 * @package    Relax
 * @subpackage Router
 * @copyright  Copyright (c) 2012, João Pinto Neto
 * @license    MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link       http://joaopintoneto.com/Relax
 * @since      Class available since Release 0.1
 */
class Relax_Router {

    public function __construct($queryString = null, $requestMethod = null) {
        $this->queryString = empty($queryString) ? $_SERVER['QUERY_STRING'] : $queryString;
        $this->requestMethod = empty($requestMethod) ? $_SERVER['REQUEST_METHOD'] : $requestMethod;
        $this->__setupDefaults();
        $this->dispatch();
    }

    private function __setupDefaults() {
        $path = explode('/', trim($this->queryString, '/'));
        $route = $path[0] ? ucfirst($path[0]) : 'Index';
        // remove extension of renderer and last slash
        $this->route = preg_replace(array('/\/$/', '/\.[a-zA-Z0-9_]+$/'), '', $route);
        $this->action = isset($path[1]) ? $path[1] : '';
        $this->params = (array) array_slice($path, 2);
    }

    private function __getRenderer() {
        preg_match('/\.([^.+]+$)/', $this->queryString, $renderer);
        return isset($renderer[1]) ? $renderer[1] : '';
    }

    private function __getRouteRules() {
        return (array) Relax_Config::read("Routes.{$this->route}");
    }

    /**
     * link:
     * https://github.com/dannyvankooten/PHP-Router
     */
    private function __parseRouteRules() {
        $uri = preg_replace(array('/\/$/', '/\.[a-zA-Z0-9_]+$/'), '', $this->queryString);
        $ruleIsFound = false;
        $rules = $this->routeRules[strtolower($this->requestMethod)];
        foreach ($rules as $rule => $action) {
            $reRule = preg_replace('/:([a-z]+)/', '(?P<\1>[a-zA-Z0-9_\-\.\!\~\*\\\'\(\)\:\@\&\=\$\+,%]+)', $rule);
            $reRule = str_replace('/', '\/', $reRule);

            if (!preg_match('@^'.$reRule.'*$@i', $uri, $matches)) {
                continue;
            } else {
                $ruleIsFound = true;
                break;
            }
        }

        if ($ruleIsFound) {
            if (is_array($action)) {
                $this->route = $action['route'];
                $this->action = $action['action'];
            } else {
                $routeActions = get_class_methods($this->route . 'Route');
                if (in_array($this->action, $routeActions)) {
                    return;

                }
                $this->action = $action;
            }

            array_shift($matches);
            $this->params = $matches;
        }

    }

    public function dispatch() {
        $routeClass = $this->route . 'Route';

        $this->routeRules = $this->__getRouteRules();
        if (!empty($this->routeRules)) {
            $this->__parseRouteRules();
        }

        $routeConfig = array(
           'action' => $this->action,
           'params' => $this->params,
           'renderTo' => $this->__getRenderer()
        );

        try {
            $route = new $routeClass($routeConfig);
            $route->render();
        } catch (Relax_HttpException $e) {
            $route = new ErrorRoute($e->getMessage(), $e->getCode());
            $route->render();
        }

    }


}
