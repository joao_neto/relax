<?php
/**
 * Datasource Database
 *
 * 
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category   Relax
 * @package    Relax_Datasource
 * @subpackage Database
 * @copyright  Copyright (c) 2012, João Pinto Neto
 * @license    MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link       http://joaopintoneto.com/Relax
 * @since      Class available since Release 0.1
 */
class Relax_Datasource_Database {
    public $conn = null;
    public $config = null;

    public function __construct($config) {
        $this->config = $config;
    }

    public function getConnection() {
        return $this->conn;
    }

    public function query($query, $params = null) {
        try {
            $statement = $this->getConnection()->prepare($query);
            $statement->execute($params);
        } catch (PDOException $e) {
            throw new Relax_HttpException('Internal Server Error', 500);
            // @todo Implement Query Log
            return false;
        }
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function select($params) {
        $this->query['fields'][] = $params;
        return $this;
    }
    
    public function from($params) {
        $this->query['from'][] = "{$params['table']} AS {$params['alias']}";
        return $this;
    }
    
    public function join($params) {
        $joins = & $this->query['joins'][];
        if (is_array($params)) {
            foreach ($params as $join) {
                    $joins[] = "{$join['type']} JOIN {$join['table']} AS {$join['alias']} ON ({$join['conditions']})";
            }
        } else {
            $joins[] = $params;
        }
        return $this;
    }
    
    public function where($params) {
        $this->query['conditions'][] = $params['conditions'];
        return $this;
    }
    
    public function order($params) {
        $this->query['order'][] = $params;
        return $this;
    }
    
    public function limit($params) {
        $limit = null;
        if (is_array($params)) {
            $limit = $params['limit'];
            if (isset($params['offset'])) {
                $limit .= ', ' . $params['offset'];
            }
        } else if (!empty($params)) {
             $limit = $params;
        }
        $this->query['limit'][] = $limit;
        return $this;
    }
}