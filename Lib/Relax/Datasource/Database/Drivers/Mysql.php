<?php
/**
 * Datasource/Database driver for Mysql
 *
 * Driver to handle MySQL database with PDO
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category   Relax
 * @package    Relax_Datasource_Database_Drivers
 * @subpackage Mysql
 * @copyright  Copyright (c) 2012, João Pinto Neto
 * @license    MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link       http://joaopintoneto.com/Relax
 * @since      Class available since Release 0.1
 */
class Relax_Datasource_Database_Drivers_Mysql extends Relax_Datasource_Database {
    public function __construct($config) {
        parent::__construct($config);
        $this->__connect();
    }

    private function __connect() {
        $default_config = array(
            'host' => 'localhost',
            'port' => 3306,
            'user' => 'root',
            'password' => '',
            'database' => 'relax',
            'driver' => 'Relax/Datasource/Database/Drivers/Mysql',
            'persistent' => false,
            'encode' => 'utf8'
        );

        $config = array_merge($default_config, $this->config);

        try {
            $this->conn = new PDO(
                "mysql:host={$config['host']};port={$config['port']};dbname={$config['database']}",
                $config['user'],
                $config['password'],
                array(
                    PDO::ATTR_PERSISTENT => $config['persistent'],
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES {$config['encode']}"
                )
            );
        } catch (PDOException $e) {
            throw new Relax_HttpException('Internal Server Error', 500);
            return false;
        }
        return $this->conn;
    }
    
    public function quote($name) {
        return '`' . $name . '`';
    }
    
    public function getQueryString($alias = null, $joinType = null) {
        $fields = & end($this->query['fields']);
        
        $query = 'SELECT ' . implode(', ', end($this->query['fields']));
        $query .= ' FROM ' . end($this->query['from']);

        if (isset($this->query['joins'])) {
            $joins = & end($this->query['joins']);
            $query .= ' ';
            $query .= implode(' ', $joins);
        }
        if (isset($this->query['where'])) {
            $conditions = & end($this->query['conditions']);
            $query .= ' WHERE ';
            $query .= implode(' AND ', $conditions);
        }
        
        if (!is_null($alias)) {
            $query = ' ( ' . $query. ' ) AS ' . $alias;
        }
        
        if (!is_null($joinType)) {
            $query = strtoupper($joinType) . ' JOIN' . $query;
        }
        
        unset($fields, $joins, $conditions);
        
        return $query;
    }
    
    
    
}