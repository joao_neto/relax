<?php
/**
 * Datasource/Database driver for Sqlite
 *
 * Driver to handle Sqlite database with PDO
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category   Relax
 * @package    Relax_Datasource_Database_Drivers
 * @subpackage Sqlite
 * @copyright  Copyright (c) 2012, João Pinto Neto
 * @license    MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link       http://joaopintoneto.com/Relax
 * @since      Class available since Release 0.1
 */
class Relax_Datasource_Database_Drivers_Sqlite extends Database {
    public function __construct($config) {
        parent::__construct($config);
        $this->__connect();
    }

    private function __connect() {
        $config = $this->config;
        try {
            $this->conn = new PDO(
                "sqlite:{$config['database']}",
                null,
                null,
                array(
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                )
            );
        } catch (PDOException $e) {
            throw new Relax_HttpException('Internal Server Error', 500);
            return false;
        }
        return $this->conn;
    }
    
    public function getQueryString($alias = null, $joinType = null) {
        $fields = & end($this->query['fields']);
        
        $query = 'SELECT ' . implode(', ', end($this->query['fields']));
        $query .= ' FROM ' . end($this->query['from']);

        if (isset($this->query['joins'])) {
            $joins = & end($this->query['joins']);
            $query .= ' ';
            $query .= implode(' ', $joins);
        }
        if (isset($this->query['where'])) {
            $conditions = & end($this->query['conditions']);
            $query .= ' WHERE ';
            $query .= implode(' AND ', $conditions);
        }
        if (isset($this->query['order'])) {
            $order = & end($this->query['order']);
            $query .= ' ORDER BY ';
            $query .= implode(', ', $order);
        }
        if (isset($this->query['limit'])) {
            $limit = & end($this->query['limit']);
            $query .= ' LIMIT ' . $limit;
        }

        if (!is_null($alias)) {
            $query = ' ( ' . $query. ' ) AS ' . $alias;
        }
        
        if (!is_null($joinType)) {
            $query = strtoupper($joinType) . ' JOIN' . $query;
        }
        
        unset($fields, $joins, $conditions, $order, $limit);
        
        return $query;
    }

    
    
    
}