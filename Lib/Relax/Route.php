<?php
/**
 * Route
 *
 * 
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category   Relax
 * @package    Relax
 * @subpackage Route
 * @copyright  Copyright (c) 2012, João Pinto Neto
 * @license    MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link       http://joaopintoneto.com/Relax
 * @since      Class available since Release 0.1
 */
class Relax_Route {
    public function __construct($config=null) {
        $this->config = $config;
        $this->name = str_replace('Route', '', get_class($this));
        $this->renderer = $this->__getRenderer();
        //$this->render();
    }

    public function __destruct() {
        unset($this->renderer);
    }

    private function __executeAction($action, $params) {
        return call_user_func_array(array(get_class($this), $action), $params);
    }

    private function __getRenderer() {
        $extension = empty($this->config['renderTo']) ? Relax_Config::read('Renderer.defaultFormat') : $this->config['renderTo'];
        $renderer = 'Relax_Renderer_' . ucfirst($extension);
        return new $renderer;
    }

    public function render() {
        $this->beforeRender();
        $action = isset($this->config['action']) ? $this->config['action'] : 'Index';
        $params = isset($this->config['params']) ? $this->config['params'] : array();
        $data = $this->__executeAction($action, $params);
        $this->renderer->render($data);
        $this->afterRender();
    }

    public function avaliableMethods() {
        return Relax_Config::read('Routes');
    }

    public function beforeRender() {

    }

    public function afterRender() {

    }

}
