<?php
/**
 * Model
 *
 * 
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category   Relax
 * @package    Relax
 * @subpackage Model
 * @copyright  Copyright (c) 2012, João Pinto Neto
 * @license    MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link       http://joaopintoneto.com/Relax
 * @since      Class available since Release 0.1
 */
class Relax_Model {
    private $__datasource = null;

    public $useConf = 'Database.default';

    public function __construct() {
        $this->__setupDatasource();
        $this->name = get_class($this);
    }

    private function __setupDatasource() {
        if (!is_null($this->__datasource)) {
            return $this->__datasource;
        }

        $Database = new Relax_Datasource(Relax_Config::read($this->useConf));
        $this->__datasource = & $Database->getDriver();
        return $this->__datasource;
    }

    public function getDatasource() {
        return $this->__datasource;
    }

    public function getConnection() {
        return $this->__setupDatasource()->getConnection();
    }

    public function query($sql, $params = null) {
        return $this->getDatasource()->query($sql, $params);
    }
    
    public function find($type, $params) {
        $from = array(
            'table' => $this->tableName,
            'alias' => $this->name
        );

        $limit = null;

        $defaultParams = array(
            'fields' => array($this->name . '.*')
        );

        if (isset($params['joins'])) {
            $joins = $params['joins'];
            foreach ($joins as $join) {
                $defaultParams['fields'][] = $join['alias'] . '.*';
            }
        }

        $params = array_merge($defaultParams, $params);

        switch (strtolower($type)) {
            case 'first':
                $limit = 1;
                break;
            case 'count':
                $params['fields'] = array('count(*) AS ct');
                break;
            default:
                break;
        }
        
        $query = $this->getDatasource();
        $query->select($params['fields'])->from($from);
        if (isset($params['joins'])) {
            $query->join($params['joins']);
        }
        $query->limit($limit);
        
        // $debug = $query->getQueryString();
        // print_r($debug);

        return $this->query($query->getQueryString());
    }


}