<?php
/**
 * Datasource
 *
 * 
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category   Relax
 * @package    Relax
 * @subpackage Datasource
 * @copyright  Copyright (c) 2012, João Pinto Neto
 * @license    MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link       http://joaopintoneto.com/Relax
 * @since      Class available since Release 0.1
 */
class Relax_Datasource {
    public $driver = null;

    public function __construct($config = null) {
        $this->__getDatasource($config);
    }

    private function __getDatasource($config = null) {
        if (is_null($config)) {
            $config = new Relax_Datasource_Database_Config(Relax_Config::read('Database.default'));
        }

        if (isset($config['driver'])) {
            $class = $config['driver'];
        } else {
            $class = str_replace('Config', '', $config);
        }

        // @todo Move strtr from here
        $class = strtr($class, '\\/.', '___');
        $this->driver = new $class($config);
    }

    public function getDriver() {
        if (!is_null($this->driver)) {
            return $this->driver;
        }
        return false;
    }
}