<?php
/**
 * Route config
 *
 * 
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category   Relax
 * @package    Relax_Route
 * @subpackage Config
 * @copyright  Copyright (c) 2012, João Pinto Neto
 * @license    MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link       http://joaopintoneto.com/Relax
 * @since      Class available since Release 0.1
 */
class Relax_Route_Config {
    public function __construct($config = null) {
        $config = (array) $config;
        foreach ($config as $prop => $value) {
            $this->{$prop} = $value;
        }
    }
}