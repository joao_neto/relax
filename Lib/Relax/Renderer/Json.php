<?php
/**
 * Json Renderer
 *
 * 
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category   Relax
 * @package    Relax_Renderer
 * @subpackage Json
 * @copyright  Copyright (c) 2012, João Pinto Neto
 * @license    MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link       http://joaopintoneto.com/Relax
 * @since      Class available since Release 0.1
 */
class Relax_Renderer_Json extends Relax_Renderer {
    public function render($data) {
        $data = (array) $data;
        echo json_encode($data);
    }
}