<?php
/**
 * Bootstrap
 *
 * 
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category   Relax
 * @copyright  Copyright (c) 2012, João Pinto Neto
 * @license    MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link       http://joaopintoneto.com/Relax
 * @since      Class available since Release 0.1
 */

if (defined('RELAX_PATH')) {
    set_include_path(RELAX_PATH . PATH_SEPARATOR . get_include_path());
}

if (!@include 'Relax/Config.php') {
    trigger_error('The path to Relax is not defined in the include_path, please set the full path of Relax in include_path or in constant RELAX_PATH', E_USER_ERROR);
}

Relax_Config::loadFile('config');

$includePaths = Relax_Config::read('IncludePaths');
if (is_array($includePaths) && !empty($includePaths)) {
    set_include_path(
        implode(PATH_SEPARATOR, $includePaths) . PATH_SEPARATOR
        . get_include_path()
    );
}

function loadClass($class) {
    if (class_exists($class, false) || interface_exists($class, false)) {
        return;
    }
    $class = strtr($class, '_/', DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR);
    include $class . '.php';
}

spl_autoload_register('loadClass');
