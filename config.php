<?php
/**
 * Application Config
 *
 * 
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @category   Relax
 * @copyright  Copyright (c) 2012, João Pinto Neto
 * @license    MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @link       http://joaopintoneto.com/Relax
 * @since      Class available since Release 0.1
 */

$config = array();

$config['Database'] = array(
    'default' => array(
        'host' => 'localhost',
        'user' => 'relax',
        'password' => '123mudar',
        'database' => 'relax',
        'driver' => 'Relax/Datasource/Database/Drivers/Mysql'
    )
);

$config['IncludePaths'] = array(
    realpath('Routes'),
    realpath('Models')
);

$config['Routes'] = array(
    'Users' => array(
        'get' => array(
            '/users' => 'getUsers',
            '/users/:id' => 'getUser',
            '/users/search/:query' => 'searchUser'
        ),
        'post' => array(
            '/users' => 'addUser'
        ),
        'put' => array(
            '/users' => 'updateUser'
        ),
        'delete' => array(
            '/users' => 'deleteUser'
        )
    )

);

$config['Renderer'] = array(
    'defaultFormat' => 'json'
);
