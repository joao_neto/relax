<?php
class ErrorRoute extends Relax_Route {
    public function __construct($message, $code = null) {
        if (empty($code)) {
            $code = 404;
        }
        $this->messageError = $message;

        $config = array(
           'action' => 'index',
           'params' => array($message, $code)
        );
        parent::__construct($config);
    }

    public function index($message, $code) {
        print_r(func_get_args());
        return json_encode(array('message' => $message, 'code' => $code));
        //return '{}';
    }
}