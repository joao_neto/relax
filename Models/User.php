<?php
class User extends Relax_Model {
    // public $useConf = 'Database.default';
    public $tableName = 'users';

    public function getUsers() {
        //return $this->query('SELECT * FROM users AS User');
        
        $options = array(
            'fields' => array(
                'User.id',
                'User.name',
            ),
            // 'from' => array(
            //     'table' => 'users',
            //     'alias' => 'User',
            // ),
            // 'joins' => array(
            //     array(
            //         'type' => 'INNER',
            //         'table' => 'users',
            //         'alias' => 'User2',
            //         'conditions' => 'User.id = User2.codigo'
            //     )
            // ),
        );
        
        return $this->find('all', $options);
    }

}